#!/usr/bin/env python3
import copy
import json
from itertools import combinations
import networkx as nx
import sys

DEBUG = True
MAX_REG = 2
WORD_SIZE = 8

# address offset for next variable to be spilled
glocation = 0
BLOCK_DICT = {}
CFG = nx.DiGraph

def color(code):
    graph = interferenceGraph(code)
    graph_save = graph.copy()

    worklist = []
    while graph.number_of_nodes() > 0:
        nodes = min_degree_nodes(graph)
        if len(graph.neighbors(nodes[0])) >= MAX_REG:
            # Select node to spill
            # Calculate costs based on original graph
            maxGain = 0
            for node in graph_save.nodes():
                # Want high impact on graph & low cost of spilling
                gain = len(graph_save.neighbors(node)) / cost(node, code)
                if gain > maxGain:
                    maxGain = gain
                    spillNode = node
            spill(spillNode, code)
            # Need to rebuild interference graph from start
            return color(code)
        worklist.append(nodes[0])
        graph.remove_node(nodes[0])

    color_dict = {}
    for node in reversed(worklist):
        colors = list_search_in_dict(graph_save.neighbors(node), color_dict)
        for i in range(MAX_REG):
            if ("RX" + str(i)) not in colors:
                color_dict[node] = "RX"+str(i)
                break

    # replace all variables with their "color" (a.k.a. register)
    for block in code["blocks"]:
        for statement in block["code"]:
            for key in statement:
                var = statement[key]
                varlist = [var]
                if var.startswith("{") or var.startswith("["):
                    varlist = var[1:-1].split(",")
                for var in varlist:
                    var = var.strip()
                    if var in color_dict:
                        statement[key] = statement[key].replace(var, color_dict[var])

# cost = SUM( 10^depth(i) ) for all i
# where i is an instruction in which v is defined or used
def cost(v, code):
    # TODO: somehow track depth
    cost, depth = 0, 0
    for block in code["blocks"]:
        for statement in block["code"]:
            for key in statement:
                if (key == "dest" or key.startswith("src")) and v in statement[key] and not statement[key].startswith("#:"):
                    cost += 10**depth
    if DEBUG:
        print("cost of", v, "is", cost)
    return cost


# TODO: We should manually load only when the argument is a left-side register,
#       as instructions can generally take the second operand from memory (?)
# TODO: We should switch operands if the operation is commutative and it helps.
# TODO: Where should we put things in memory????
def spill(v, code):
    '''
    Spill variable "v" everywhere.
    Makes a copy of "code" to iterate over - could be slightly more efficent.
    In general, stmtNum+stmtNumOffset = actual position of current statement
    '''
    if DEBUG:
        print("----------- Spilling", v, "------------")
    # Assign a memory location for the spilled variable
    global glocation
    location = glocation
    glocation += WORD_SIZE

    # append a subscript - each new temporary should be unique
    subscript = 0
    code_copy = copy.deepcopy(code)
    for blockNum, block in enumerate(code_copy["blocks"]):
        # every load or store we insert will bump stmtNum up, although the for
        # loop will give the original index. Track the offset...
        stmtNumOffset = 0
        for stmtNum, statement in enumerate(block["code"]):
            defined, used = definedAndUsed(statement)
            if v in used:
                # insert LOAD before every use
                load = {
                    "op": "ldr",
                    "dest": "T"+str(location)+"_"+str(subscript),
                    "src1": "[fp, #XX"+str(location)+"]", # TODO: ????
                    "src2": "",
                }
                code["blocks"][blockNum]["code"].insert(stmtNum + stmtNumOffset, load)
                stmtNumOffset += 1
            if v in used or v in defined:
                # adjust instruction to use the newly created temporary
                origStatement = code["blocks"][blockNum]["code"][stmtNum + stmtNumOffset]
                for key in origStatement:
                    origStatement[key] = origStatement[key].replace(v, "T"+str(location)+"_"+str(subscript))
                subscript += 1
                if DEBUG:
                    print(origStatement)
            if v in defined:
                # insert STORE after every change
                store = {
                    "op": "str",
                    "dest": "T"+str(location)+"_"+str(subscript-1),
                    "src1": "[fp, #XX"+str(location)+"]", # TODO: ????
                    "src2": "",
                }
                code["blocks"][blockNum]["code"].insert(stmtNum + stmtNumOffset + 1, store)
                stmtNumOffset += 1

def buildCFG(code):
    '''
    Set up the CFG and BLOCK_DICT globals
    '''
    global BLOCK_DICT
    global CFG
    BLOCK_DICT = {}
    CFG = nx.DiGraph()
    for block in code["blocks"]:
        BLOCK_DICT[block["name"]] = block
        for succ in block["sucessor"]:
            CFG.add_edge(block["name"], succ)

def definedAndUsed(stmt):
    '''
    Find which variables are defined and which are used.
    "dest" might not actually be the destination, such as for "str", and with
    "ldmfd" the destinations are listed in "src1"!
    '''
    defined, used = [], []
    srcs = []
    # variables that we are not allowed to reassign
    ignoreVars = ["sp", "fp", "pc", "lr"]

    if "dest" in stmt:
        dest = stmt["dest"]
        if dest.endswith('!'):
            dest = dest[:-1]
        if dest not in ignoreVars:
            if stmt["op"] == "str":
                used.append(dest)
            else:
                defined.append(dest)
            # should also have exception for ldmfd/stmfd but...
            # ...only used with sp in practice, which we ignore
    for key in stmt:
        if key.startswith("src"):
            if stmt[key].startswith("{"):
                for src in stmt[key][1:-1].split(","):
                    srcs.append(src)
            elif stmt[key].startswith('['):
                srcs.append(stmt[key][1:-1].split(",")[0])
            elif not stmt[key] == "":
                srcs.append(stmt[key])
    for src in srcs:
        src = src.strip()
        if not src.startswith('#') and src not in ignoreVars:
            if stmt["op"] == "ldmfd":
                defined.append(src)
            else:
                used.append(src)
    return set(defined), set(used)

def global_liveness(code):
    # first reset block attributes from previous (stale) analyses
    for block in code["blocks"]:
        del block["livein"]
    changed = True
    while changed:# If any livein is changed iterate again
        changed = False
        worklist = [n["name"] for n in code["blocks"] if n["next"]==[]] # Leaf nodes
        while len(worklist) != 0:
            node = BLOCK_DICT[worklist[0]]
            worklist = worklist[1:]

            live = set()
            for succ in node["sucessor"]:
                b = BLOCK_DICT[succ]
                if "livein" in b:
                    live |= set(b["livein"])
            node["liveout"] = list(live)

            for statement in reversed(node["code"]):
                defined, used = definedAndUsed(statement)
                live -= defined
                live |= used

            blockChanged = "livein" not in node or set(node["livein"]) != live
            changed |= blockChanged
            node["livein"] = list(live)

            if not blockChanged:
                continue

            for pred in node["predessesor"]:
                if pred not in worklist:
                    worklist.append(pred)

            if DEBUG:
                print(worklist)


def interferenceGraph(code):
    """
    Build a liveness interference graph.

    """
    if DEBUG:
        print(" --- waiting for enter ---")
        sys.stdin.read(1)
    global_liveness(code)

    graph = nx.Graph()
    for block in code["blocks"]:
        live = set(block["liveout"])
        for statement in reversed(block["code"]):

            defined, used = definedAndUsed(statement)
            live -= defined
            live |= used

            if len(live) > 1:
                graph.add_edges_from(combinations(live, 2))
            else:
                graph.add_nodes_from(live)
    if DEBUG:
        for node in sorted(graph.nodes()):
            print(node, "has", graph.degree(node), "neighbors", graph.neighbors(node))
    return graph

def min_degree_nodes(graph):
    ret=[]
    minn = graph.number_of_nodes()
    for node in graph.nodes():
        if graph.degree(node) <= minn:
            minn = graph.degree(node)
    for node in graph.nodes():
        if graph.degree(node) == minn:
            ret.append(node)
    return ret

def list_search_in_dict(listt, dictt):
    ret = []
    for element in listt:
        if dictt.get(element):
            ret.append(dictt.get(element))
    return ret

def main():
    with open('output.json') as input:
        code = json.loads(input.read())
        buildCFG(code)
        color(code)
        print(json.dumps(code, indent=4))


if __name__ == "__main__":
    main()
